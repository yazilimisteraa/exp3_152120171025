/*
 * Circle.cpp
 *
 *  Created on: 8 Kas 2018
 *      Author: ykartal
 */

#include "Circle.h"
using namespace std;
	Circle::Circle(double r){
		setR(r);
	}

	Circle::~Circle() {
	}

	void Circle::setR(double r) {
		this->r = r;
	}
   void Circle::setR(int r) {
		this->r = r;
	}
	double Circle::getR()const {
		return r;
	}

	double Circle::calculateCircumference()const {
		return PI * r * r;
	}

	double Circle::calculateArea() {
		return PI * r * 2;
	}
	bool Circle::ifequal(Circle &c)
	{
		if (c.r == r)
			return true;
		else
		return false;
	}
